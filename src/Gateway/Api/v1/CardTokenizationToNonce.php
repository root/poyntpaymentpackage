<?php

namespace Devsolutions\PoyntPayment\Gateway\Api\v1;

use GuzzleHttp\Client;
use Devsolutions\PoyntPayment\Gateway\Models\Card;
use Ramsey\Uuid\Nonstandard\Uuid;

class CardTokenizationToNonce
{
    protected Card $card;
    protected $environment;
    protected $endpoint;
    protected $baseUrl;
    protected $businessId;
    protected $appId;
    protected $uuid;


    public function __construct(Card $card, $environment, $businessId, $appId)
    {
        $this->baseUrl = env('PROD_GATEWAY_BASE_URL');
        $this->businessId = env('BUSINESS_ID');
        $this->appId = env('APP_ENV');
        $this->card = $card;
        $this->uuid = Uuid::uuid4();
        $this->endpoint = $this->baseUrl . '/businesses/'.$this->businessId.'/cards/open-tokenize';
    }

    protected function payload() : array
    {
        return [
            'applicationId' => $this->appId,
            'card' => [
                'number' => $this->card->getNumber(),
                'expirationMonth' => $this->card->getExpirationMonth(),
                'expirationYear' => $this->card->getExpirationYear(),
                'type' => $this->card->gettype(),
                'cardHolderFirstName' => $this->card->getCardHolderFirstName(),
                'cardHolderLastName' => $this->card->getCardHolderLastName()
            ],
            'verificationData' => [
                'cvData' => $this->card->getCvData(),
                'cardHolderBillingAddress' => [
                    'postalCode' => $this->card->getPostalCode(),
                    'line1' => $this->card->getLine1()
                ]
            ]
        ];
    }

    protected function headers() : array
    {
        return [
            'Content-Type' => 'application/json; charset=utf-8',
            'Poynt-Request-Id' => (string) $this->uuid
        ];
    }

    public function getNonce()
    {
        $client = new Client([
            'headers' => $this->headers()
        ]);

        $request = $client->request('POST', $this->endpoint, [
            'body' => json_encode($this->payload())
        ]);
        return json_decode($request->getBody()->getContents(), true)['nonce'];
    }
}

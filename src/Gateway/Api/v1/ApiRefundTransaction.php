<?php

namespace Devsolutions\PoyntPayment\Gateway\Api\v1;

use GuzzleHttp\Client;
use Ramsey\Uuid\Uuid;

class ApiRefundTransaction
{
    protected $environment;
    protected $baseUrl;
    protected $businessId;
    protected $transactionId;
    protected $transactionAmount;
    protected $orderAmount;
    protected $notes;
    protected $authToken;
    protected $endpoint;
    const ACTION = 'REFUND';
    const CURRENCY = 'USD';

    /**
     * @param $environment
     * @param $baseUrl
     * @param $businessId
     * @param $action
     * @param $transactionId
     * @param $transactionAmount
     * @param $orderAmount
     * @param $currency
     * @param $notes
     */
    public function __construct($environment, $authToken, $businessId, $transactionId, $transactionAmount, $orderAmount, $notes)
    {
        $this->environment = env('APP_ENV');
        $this->baseUrl = env('PROD_GATEWAY_BASE_URL');
        $this->businessId = env('BUSINESS_ID');
        $this->endpoint = $this->baseUrl . '/businesses/'.$this->businessId.'/transactions';
        $this->transactionId = $transactionId;
        //dd($this->transactionId);
        $this->transactionAmount = $transactionAmount;
        $this->orderAmount = $orderAmount;
        $this->notes = $notes;
        $this->authToken = $authToken;
    }

    protected function headers() : array
    {
        return [
            'Poynt-Request-Id' => (string) Uuid::uuid4(),
            'Authorization' => 'Bearer '. $this->authToken,
            'Content-Type' => 'application/json'
        ];
    }

    protected function payload() : array
    {
        return [
            'action' => 'REFUND',
            'parentId' => $this->transactionId,
            'id' => (string) Uuid::uuid4(),
            "amounts" => [
                "transactionAmount" => $this->transactionAmount,
                "orderAmount" => $this->orderAmount,
                "currency" => "USD",
            ]
        ];
    }

    public function refundATransaction()
    {
        $client = new Client([
            'headers' => $this->headers()
        ]);

        $request = $client->request('POST', $this->endpoint, [
            'body' => json_encode($this->payload())
        ]);

        // TODO: Refund Model Object

        return json_decode($request->getBody()->getContents(), true);
    }
}

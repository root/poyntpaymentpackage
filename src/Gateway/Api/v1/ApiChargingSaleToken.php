<?php

namespace Devsolutions\PoyntPayment\Gateway\Api\v1;

use GuzzleHttp\Client;
use Devsolutions\PoyntPayment\Gateway\Models\ProcessorResponse;
use Devsolutions\PoyntPayment\Gateway\Models\TokenizedCard;
use Devsolutions\PoyntPayment\Gateway\Models\Transaction;
use Ramsey\Uuid\Uuid;

class ApiChargingSaleToken
{
    protected $baseUrl;
    protected $endpoint;
    protected $businessId;
    protected $environment;
    protected $cardToken;
    protected $authToken;
    protected $sourceApp;
    protected $transactionAmount;
    protected $orderAmount;
    const CURRENCY = 'USD';
    protected $chargeType;

    /**
     * @param $baseUrl
     * @param $endpoint
     * @param $businessId
     * @param $environment
     * @param $cardToken
     * @param $authToken
     * @param $sourceApp
     * @param $transactionAmount
     * @param $orderAmount
     * @param $currency
     */
    public function __construct($businessId, $environment, $cardToken, $authToken, $sourceApp, $transactionAmount, $orderAmount, $chargeType)
    {
        $this->baseUrl = env('PROD_GATEWAY_BASE_URL');
        $this->businessId = $businessId;
        $this->environment = $environment;
        $this->endpoint = $this->baseUrl . '/businesses/'.$this->businessId.'/cards/tokenize/charge';
        $this->cardToken = $cardToken;
        $this->authToken = $authToken;
        $this->sourceApp = $sourceApp;
        $this->transactionAmount = $transactionAmount;
        $this->orderAmount = $orderAmount;
        $this->chargeType = $chargeType;
    }


    protected function headers()
    {
        return [
            'Poynt-Request-Id' => (string) Uuid::uuid4(),
            'Authorization' => 'Bearer '. $this->authToken->getAccessToken(),
            'Content-Type' => 'application/json'
        ];
    }

    protected function payload()
    {
        return [
            'action' => 'SALE',
            'context' => [
                'businessId' => $this->businessId,
                'sourceApp' => $this->sourceApp
            ],
            'amounts' => [
                'transactionAmount' => $this->transactionAmount,
                'orderAmount' => $this->orderAmount,
                'currency' => "USD",
            ],
            'fundingSource' => [
                'cardToken' => $this->cardToken,
            ],
            'emailReceipt' => true,
            'receiptEmailAddress' => 'ab@devsolutions.team',
        ];
    }

    protected function getTokenizedCard($response) : TokenizedCard
    {
        $tokenizedCard = new TokenizedCard(
            null,
            $response['fundingSource']['card']['id'],
            $response['fundingSource']['card']['type'],
            $response['fundingSource']['card']['source'],
            $response['fundingSource']['card']['expirationMonth'],
            $response['fundingSource']['card']['expirationYear'],
            $response['fundingSource']['card']['numberFirst6'],
            $response['fundingSource']['card']['numberLast4'],
            $response['fundingSource']['card']['numberMasked'],
            $response['fundingSource']['card']['numberHashed'],
            $response['fundingSource']['card']['cardHolderFirstName'],
            $response['fundingSource']['card']['cardHolderLastName'],
            $response['fundingSource']['card']['cardId'],
            $response['customerUserId'],
            $response['fundingSource']['cardToken'],
            $response['fundingSource']['card']['status'],
            null
        );

        return $tokenizedCard;
    }

    private function getProcessingResponse($response) : ProcessorResponse
    {
        $processingResponse = new ProcessorResponse(
            $response['processorResponse']['approvedAmount'],
            $response['processorResponse']['processor'],
            $response['processorResponse']['acquirer'],
            $response['processorResponse']['status'],
            $response['processorResponse']['statusCode'],
            $response['processorResponse']['statusMessage'],
            $response['processorResponse']['transactionId'],
            $response['processorResponse']['approvalCode'],
            $response['processorResponse']['batchId'],
            $response['processorResponse']['retrievalRefNum'],
        );

        return $processingResponse;
    }

    protected function getTransaction($response) : Transaction
    {
        $transaction = new Transaction(
            $response['id'],
            $response['status'],
            $response['createdAt'],
            $response['updatedAt'],
            $response['context']['businessType'],
            $response['context']['transmissionAtLocal'],
            $response['context']['storeDeviceId'],
            $response['context']['sourceApp'],
            $response['context']['mcc'],
            $response['context']['source'],
            $response['context']['storeId'],
            $response['fundingSource']['debit'],
            $this->getTokenizedCard($response),
            $response['customerUserId'],
            $response['fundingSource']['cardToken'],
            $response['action'],
            $response['amounts']['transactionAmount'],
            $response['amounts']['orderAmount'],
            $response['amounts']['tipAmount'],
            $response['amounts']['cashbackAmount'],
            $response['amounts']['currency'],
            $this->getProcessingResponse($response)
        );

        return $transaction;
    }

    public function callChargeCard() 
    {
        $client = new Client([
            'headers' => $this->headers()
        ]);

        $request = $client->request('POST', $this->endpoint, [
            'body' => json_encode($this->payload())
        ]);

        $response = json_decode($request->getBody()->getContents(), true);

        return $response;
    }

}

<?php

namespace Devsolutions\PoyntPayment\Gateway\Api\v1;

use GuzzleHttp\Client;
use Devsolutions\PoyntPayment\Gateway\Models\TokenizedCard;
use Ramsey\Uuid\Uuid;

class ApiCardTokenization
{
    protected $nonce;
    protected $businessId;
    protected $uuid;

    protected $environment;
    protected $baseUrl;
    protected $endpoint;
    protected $authToken;
    protected TokenizedCard $tokenizedCard;

    public function __construct($environment, $businessId, $nonce, $authToken)
    {
        $this->environment = $environment;
        $this->baseUrl = env('PROD_GATEWAY_BASE_URL');
        $this->nonce = $nonce;
        $this->businessId = $businessId;
        $this->uuid = (string) Uuid::uuid4();
        $this->endpoint = 'https://services.poynt.net'. '/businesses/' . $this->businessId . '/cards/tokenize';
        $this->authToken = $authToken;
    }

    protected function headers() : array
    {
        return [
            'Poynt-Request-Id' => $this->uuid,
            'Authorization' => 'Bearer ' . $this->authToken,
            'Content-Type' => 'application/json'
        ];
    }

    protected function payload() : array
    {
        return [
            'nonce' => $this->nonce
        ];
    }

    public function getTokenizedCard() : TokenizedCard
    {
        return $this->tokenizedCard;
    }

    public function tokenizeANonce() {
        $client = new Client([
            'headers' => $this->headers()
        ]);

        $request = $client->request('POST', $this->endpoint, [
            'body' => json_encode($this->payload()),
        ]);

        $rawTokenizedCard = json_decode($request->getBody()->getContents(), true);

        $tokenizedCard = new TokenizedCard(
            $this->nonce,
            $rawTokenizedCard['card']['id'],
            $rawTokenizedCard['card']['type'],
            $rawTokenizedCard['card']['source'],
            $rawTokenizedCard['card']['expirationMonth'],
            $rawTokenizedCard['card']['expirationYear'],
            $rawTokenizedCard['card']['numberFirst6'],
            $rawTokenizedCard['card']['numberLast4'],
            $rawTokenizedCard['card']['numberMasked'],
            $rawTokenizedCard['card']['numberHashed'],
            $rawTokenizedCard['card']['cardHolderFirstName'],
            $rawTokenizedCard['card']['cardHolderLastName'],
            $rawTokenizedCard['card']['cardId'],
            $rawTokenizedCard['customerUserId'],
            $rawTokenizedCard['paymentToken'],
            $rawTokenizedCard['status'],
            $rawTokenizedCard['token']
        );

        $this->tokenizedCard = $tokenizedCard;
    }
}

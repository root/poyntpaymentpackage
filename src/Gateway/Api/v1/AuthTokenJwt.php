<?php

namespace Devsolutions\PoyntPayment\Gateway\Api\v1;

use GuzzleHttp\Client;
use Devsolutions\PoyntPayment\Gateway\Models\AuthToken;
use Ramsey\Uuid\Uuid;

class AuthTokenJwt
{
    protected $baseUrl;
    protected $endpoint;
    protected $environment;
    protected $slefSignedJwt;
    protected $uuid;
    protected $contentType;


    public function __construct($environment, $selfSignedJwt)
    {
        $this->endpoint = env('PROD_GATEWAY_BASE_URL').'/token';
        $this->environment = env('APP_ENV');
        $this->slefSignedJwt = $selfSignedJwt;
        $this->uuid = (string) Uuid::uuid4();
        $this->contentType = 'application/x-www-form-urlencoded';
    }

    protected function headers(): array
    {
        return [
            'BUSINESS_ID' =>  config('poynt-keys.businessId'),
            'Content-Type' => 'application/x-www-form-urlencoded'
        ];
    }

    protected function payload() : array
    {
        return [
            'grantType' => 'urn:ietf:params:oauth:grant-type:jwt-bearer',
            'assertion' => $this->slefSignedJwt
        ];
    }

    public function getAuthToken() : AuthToken
    {
        $client = new Client([
            'headers' => $this->headers()
        ]);

        $request = $client->post($this->endpoint, [
            'form_params' => $this->payload()
        ]);

        $response = json_decode($request->getBody()->getContents());

        $authToken = new AuthToken($response->expiresIn, $response->accessToken, $response->refreshToken, $response->scope, $response->tokenType);
        return $authToken;
    }
}

<?php

namespace Devsolutions\PoyntPayment\Gateway\Api\v1;

use GuzzleHttp\Client;
use Devsolutions\PoyntPayment\Gateway\Models\VoidResponse;
use Ramsey\Uuid\Uuid;

class ApiCardVoid
{
    protected $environment;
    protected $baseUrl;
    protected $endpoint;
    protected $businessId;
    protected $appId;
    protected $authToken;
    protected $transactionId;

    /**
     * @param $environment
     * @param $baseUrl
     * @param $endpoint
     * @param $businessId
     * @param $appId
     * @param $authToken
     * @param $transactionId
     */
    public function __construct($environment, $businessId, $appId, $authToken, $transactionId)
    {
        $this->environment = $environment;
        $this->baseUrl = env('PROD_GATEWAY_BASE_URL');
        $this->businessId = $businessId;
        $this->appId = $appId;
        $this->authToken = $authToken;
        $this->transactionId = $transactionId;
        $this->endpoint = $this->baseUrl . '/businesses/'.$this->businessId.'/transactions/'.$this->transactionId.'/void';
    }

    protected function headers() : array
    {
        return [
            'Poynt-Request-Id' => (string) Uuid::uuid4(),
            'Authorization' => 'Bearer '. $this->authToken,
        ];
    }

    public function voidCard()
    {
        $client = new Client([
            'headers' => $this->headers()
        ]);

        $request = $client->request('POST', $this->endpoint, []);

        $poyntResponse =  json_decode($request->getBody()->getContents(), true);

    /*    $response  = new  VoidResponse(
            $poyntResponse['status'],
            $poyntResponse['processorResponse']['statusCode'],
            $poyntResponse['processorResponse']['statusMessage'],
            $poyntResponse['processorResponse']['transactionId'],
            $poyntResponse['action'],
            $poyntResponse['amounts']['transactionAmount'],
            $poyntResponse['amounts']['orderAmount'],
            $poyntResponse['amounts']['currency'],
        );*/

        return $poyntResponse;
    }


}

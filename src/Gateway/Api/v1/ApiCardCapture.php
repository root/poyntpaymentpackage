<?php

namespace Devsolutions\PoyntPayment\Gateway\Api\v1;

use GuzzleHttp\Client;
use Devsolutions\PoyntPayment\Gateway\Models\CaptureResponse;
use Ramsey\Uuid\Uuid;

class ApiCardCapture
{
    protected $environment;
    protected $baseUrl;
    protected $endpoint;
    protected $businessId;
    protected $transactionId;
    protected $authToken;

    /**
     * @param $environment
     * @param $businessId
     * @param $transactionId
     * @param $authToken
     */
    public function __construct($environment, $businessId,$transactionId,$authToken)
    {
        $this->environment = $environment;
        $this->baseUrl = env('PROD_GATEWAY_BASE_URL');
        $this->businessId = $businessId;
        $this->authToken = $authToken;
        $this->transactionId = $transactionId;
        $this->endpoint = $this->baseUrl . '/businesses/'.$this->businessId.'/transactions/'.$this->transactionId.'/capture';
    }

    protected function headers() : array
    {
        return [
            'Poynt-Request-Id' => (string) Uuid::uuid4(),
            'Authorization' => 'Bearer '. $this->authToken,
        ];
    }

    public function cardCapture()
    {
        $client = new Client([
            'headers' => $this->headers()
        ]);

        $request = $client->request('POST', $this->endpoint, []);

        $poyntResponse =  json_decode($request->getBody()->getContents(), true);

        return $poyntResponse;
    }
}

<?php

namespace Devsolutions\PoyntPayment\Gateway\Api\v1;

use GuzzleHttp\Client;
use Devsolutions\PoyntPayment\Gateway\Models\RefreshAuthToken;
use Ramsey\Uuid\Uuid;

class RefreshAuthTokenJwt
{
    protected $baseUrl;
    protected $endpoint;
    protected $environment;
    protected  $refreshToken;
    protected $uuid;
    protected $contentType;


    public function __construct($environment, $refreshToken)
    {
        $this->endpoint = env('APP_ENV').'/token';
        $this->baseUrl = env('APP_ENV');
        $this->environment = $environment;
        $this->refreshToken = $refreshToken;
        $this->uuid = (string) Uuid::uuid4();
        $this->contentType = 'application/x-www-form-urlencoded';
    }

    protected function headers(): array
    {
        return [
            'BUSINESS_ID' =>  config('poynt-keys.businessId'),
            'Content-Type' => 'application/x-www-form-urlencoded'
        ];
    }

    protected function payload() : array
    {
        return [
            'grantType' => 'REFRESH_TOKEN',
            'refreshToken' => $this->refreshToken
        ];
    }

    public function getRefreshAuthToken() : RefreshAuthToken
    {
        $client = new Client([
            'headers' => $this->headers()
        ]);

        $request = $client->post($this->endpoint, [
            'form_params' => $this->payload()
        ]);

        $response = json_decode($request->getBody()->getContents());

        $refreshAuthToken = new RefreshAuthToken($response->accessToken, $response->refreshToken, $response->tokenType);
        return $refreshAuthToken;
    }
}

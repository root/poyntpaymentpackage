<?php

namespace Devsolutions\PoyntPayment\Gateway\Models;

class TokenizedCard
{
    protected $nonce;
    protected $id;
    protected $type;
    protected $source;
    protected $expirationMonth;
    protected $expirationYear;
    protected $numberFirst6;
    protected $numberLast4;
    protected $numberMasked;
    protected $numberHashed;
    protected $cardHolderFirstName;
    protected $cardHolderLastName;
    protected $cardId;
    protected $customerUserId;
    protected $paymentToken;
    protected $status;
    protected $token;

    /**
     * @param $nonce
     * @param $id
     * @param $type
     * @param $source
     * @param $expirationMonth
     * @param $expirationYear
     * @param $numberFirst6
     * @param $numberLast4
     * @param $numberMasked
     * @param $numberHashed
     * @param $cardHolderFirstName
     * @param $cardHolderLastName
     * @param $cardId
     * @param $customerUserId
     * @param $paymentToken
     * @param $status
     * @param $token
     */
    public function __construct($nonce, $id, $type, $source, $expirationMonth, $expirationYear, $numberFirst6, $numberLast4, $numberMasked, $numberHashed, $cardHolderFirstName, $cardHolderLastName, $cardId, $customerUserId, $paymentToken, $status, $token)
    {
        $this->nonce = $nonce;
        $this->id = $id;
        $this->type = $type;
        $this->source = $source;
        $this->expirationMonth = $expirationMonth;
        $this->expirationYear = $expirationYear;
        $this->numberFirst6 = $numberFirst6;
        $this->numberLast4 = $numberLast4;
        $this->numberMasked = $numberMasked;
        $this->numberHashed = $numberHashed;
        $this->cardHolderFirstName = $cardHolderFirstName;
        $this->cardHolderLastName = $cardHolderLastName;
        $this->cardId = $cardId;
        $this->customerUserId = $customerUserId;
        $this->paymentToken = $paymentToken;
        $this->status = $status;
        $this->token = $token;
    }

    /**
     * @return mixed
     */
    public function getNonce()
    {
        return $this->nonce;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return mixed
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * @return mixed
     */
    public function getExpirationMonth()
    {
        return $this->expirationMonth;
    }

    /**
     * @return mixed
     */
    public function getExpirationYear()
    {
        return $this->expirationYear;
    }

    /**
     * @return mixed
     */
    public function getNumberFirst6()
    {
        return $this->numberFirst6;
    }

    /**
     * @return mixed
     */
    public function getNumberLast4()
    {
        return $this->numberLast4;
    }

    /**
     * @return mixed
     */
    public function getNumberMasked()
    {
        return $this->numberMasked;
    }

    /**
     * @return mixed
     */
    public function getNumberHashed()
    {
        return $this->numberHashed;
    }

    /**
     * @return mixed
     */
    public function getCardHolderFirstName()
    {
        return $this->cardHolderFirstName;
    }

    /**
     * @return mixed
     */
    public function getCardHolderLastName()
    {
        return $this->cardHolderLastName;
    }

    /**
     * @return mixed
     */
    public function getCardId()
    {
        return $this->cardId;
    }

    /**
     * @return mixed
     */
    public function getCustomerUserId()
    {
        return $this->customerUserId;
    }

    /**
     * @return mixed
     */
    public function getPaymentToken()
    {
        return $this->paymentToken;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return mixed
     */
    public function getToken()
    {
        return $this->token;
    }




}

<?php

namespace Devsolutions\PoyntPayment\Gateway\Models;

class VoidResponse {
    protected $status; //String
    protected $statusCode; //String
    protected $statusMessage; //String
    protected $transactionId; //String
    protected $action; //String
    protected $transactionAmount; //String
    protected $orderAmount; //String
    protected $currency; //String

    public function __construct($status, $statusCode, $statusMessage, $transactionId, $action,$transactionAmount,$orderAmount,$currency)
    {
        $this->status = $status;
        $this->statusCode = $statusCode;
        $this->statusMessage = $statusMessage;
        $this->transactionId = $transactionId;
        $this->action = $action;
        $this->transactionAmount = $transactionAmount;
        $this->orderAmount = $orderAmount;
        $this->currency = $currency;
    }

}

<?php

namespace Devsolutions\PoyntPayment\Gateway\Models;

class ProcessorResponse {
    protected $approvedAmount; //int
    protected $processor; //String
    protected $acquirer; //String
    protected $status; //String
    protected $statusCode; //String
    protected $statusMessage; //String
    protected $transactionId; //String
    protected $approvalCode; //String
    protected $batchId; //String
    protected $retrievalRefNum; //String

    /**
     * @param $approvedAmount
     * @param $processor
     * @param $acquirer
     * @param $status
     * @param $statusCode
     * @param $statusMessage
     * @param $transactionId
     * @param $approvalCode
     * @param $batchId
     * @param $retrievalRefNum
     */
    public function __construct($approvedAmount, $processor, $acquirer, $status, $statusCode, $statusMessage, $transactionId, $approvalCode, $batchId, $retrievalRefNum)
    {
        $this->approvedAmount = $approvedAmount;
        $this->processor = $processor;
        $this->acquirer = $acquirer;
        $this->status = $status;
        $this->statusCode = $statusCode;
        $this->statusMessage = $statusMessage;
        $this->transactionId = $transactionId;
        $this->approvalCode = $approvalCode;
        $this->batchId = $batchId;
        $this->retrievalRefNum = $retrievalRefNum;
    }

    /**
     * @return mixed
     */
    public function getApprovedAmount()
    {
        return $this->approvedAmount;
    }

    /**
     * @param mixed $approvedAmount
     */
    public function setApprovedAmount($approvedAmount): void
    {
        $this->approvedAmount = $approvedAmount;
    }

    /**
     * @return mixed
     */
    public function getProcessor()
    {
        return $this->processor;
    }

    /**
     * @param mixed $processor
     */
    public function setProcessor($processor): void
    {
        $this->processor = $processor;
    }

    /**
     * @return mixed
     */
    public function getAcquirer()
    {
        return $this->acquirer;
    }

    /**
     * @param mixed $acquirer
     */
    public function setAcquirer($acquirer): void
    {
        $this->acquirer = $acquirer;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status): void
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * @param mixed $statusCode
     */
    public function setStatusCode($statusCode): void
    {
        $this->statusCode = $statusCode;
    }

    /**
     * @return mixed
     */
    public function getStatusMessage()
    {
        return $this->statusMessage;
    }

    /**
     * @param mixed $statusMessage
     */
    public function setStatusMessage($statusMessage): void
    {
        $this->statusMessage = $statusMessage;
    }

    /**
     * @return mixed
     */
    public function getTransactionId()
    {
        return $this->transactionId;
    }

    /**
     * @param mixed $transactionId
     */
    public function setTransactionId($transactionId): void
    {
        $this->transactionId = $transactionId;
    }

    /**
     * @return mixed
     */
    public function getApprovalCode()
    {
        return $this->approvalCode;
    }

    /**
     * @param mixed $approvalCode
     */
    public function setApprovalCode($approvalCode): void
    {
        $this->approvalCode = $approvalCode;
    }

    /**
     * @return mixed
     */
    public function getBatchId()
    {
        return $this->batchId;
    }

    /**
     * @param mixed $batchId
     */
    public function setBatchId($batchId): void
    {
        $this->batchId = $batchId;
    }

    /**
     * @return mixed
     */
    public function getRetrievalRefNum()
    {
        return $this->retrievalRefNum;
    }

    /**
     * @param mixed $retrievalRefNum
     */
    public function setRetrievalRefNum($retrievalRefNum): void
    {
        $this->retrievalRefNum = $retrievalRefNum;
    }


}

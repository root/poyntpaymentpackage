<?php

namespace Devsolutions\PoyntPayment\Gateway\Models;

class RefreshAuthToken
{
    protected $accessToken;
    protected $tokenType;

    protected $refreshToken;

    public function __construct($accessToken, $refreshToken, $tokenType)
    {
        $this->accessToken = $accessToken;
        $this->refreshToken = $refreshToken;
        $this->tokenType = $tokenType;
    }

    /**
     * Get the value of accessToken
     */
    public function getAccessToken()
    {
        return $this->accessToken;
    }

    /**
     * Set the value of accessToken
     *
     * @return  self
     */
    public function setAccessToken($accessToken)
    {
        $this->accessToken = $accessToken;

        return $this;
    }

    /**
     * Get the value of tokenType
     */
    public function getTokenType()
    {
        return $this->tokenType;
    }

    /**
     * Set the value of tokenType
     *
     * @return  self
     */
    public function setTokenType($tokenType)
    {
        $this->tokenType = $tokenType;

        return $this;
    }

    /**
     * Get the value of refreshToken
     */
    public function getRefreshToken()
    {
        return $this->refreshToken;
    }

    /**
     * Set the value of refreshToken
     *
     * @return  self
     */
    public function setRefreshToken($refreshToken)
    {
        $this->refreshToken = $refreshToken;

        return $this;
    }
}

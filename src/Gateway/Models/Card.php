<?php

namespace Devsolutions\PoyntPayment\Gateway\Models;



use Devsolutions\PoyntPayment\Helpers\CardTypeHelper;

class Card
{
    protected $number; // card number
    protected $expirationMonth; // card expire month 2 digits 01 => jan
    protected $expirationYear; // card expire year
    protected $type; // card type
    protected $cardHolderFirstName; // card holder first anem
    protected $cardHolderLastName; // card holder last name
    protected $cvData; // cvc secret number
    protected $postalCode; // card holder postal code
    protected $line1; // card holder address

    public function __construct($number, $expirationMonth, $expirationYear, $cardHolderFirstName, $cardHolderLastName, $cvData, $postalCode, $line1)
    {
        $this->number = $number;
        $this->expirationMonth = $expirationMonth;
        $this->expirationYear = $expirationYear;
        $this->type = CardTypeHelper::getType($number);
        $this->cardHolderFirstName = $cardHolderFirstName;
        $this->cardHolderLastName = $cardHolderLastName;
        $this->cvData = $cvData;
        $this->postalCode = $postalCode;
        $this->line1 = $line1;
    }

    /**
     * Get the value of number
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set the value of number
     *
     * @return  self
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get the value of expirationMonth
     */
    public function getExpirationMonth()
    {
        return $this->expirationMonth;
    }

    /**
     * Set the value of expirationMonth
     *
     * @return  self
     */
    public function setExpirationMonth($expirationMonth)
    {
        $this->expirationMonth = $expirationMonth;

        return $this;
    }

    /**
     * Get the value of expirationYear
     */
    public function getExpirationYear()
    {
        return $this->expirationYear;
    }

    /**
     * Set the value of expirationYear
     *
     * @return  self
     */
    public function setExpirationYear($expirationYear)
    {
        $this->expirationYear = $expirationYear;

        return $this;
    }

    /**
     * Get the value of type
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set the value of type
     *
     * @return  self
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get the value of cardHolderFirstName
     */
    public function getCardHolderFirstName()
    {
        return $this->cardHolderFirstName;
    }

    /**
     * Set the value of cardHolderFirstName
     *
     * @return  self
     */
    public function setCardHolderFirstName($cardHolderFirstName)
    {
        $this->cardHolderFirstName = $cardHolderFirstName;

        return $this;
    }

    /**
     * Get the value of cardHolderLastName
     */
    public function getCardHolderLastName()
    {
        return $this->cardHolderLastName;
    }

    /**
     * Set the value of cardHolderLastName
     *
     * @return  self
     */
    public function setCardHolderLastName($cardHolderLastName)
    {
        $this->cardHolderLastName = $cardHolderLastName;

        return $this;
    }

    /**
     * Get the value of cvData
     */
    public function getCvData()
    {
        return $this->cvData;
    }

    /**
     * Set the value of cvData
     *
     * @return  self
     */
    public function setCvData($cvData)
    {
        $this->cvData = $cvData;

        return $this;
    }

    /**
     * Get the value of postalCode
     */
    public function getPostalCode()
    {
        return $this->postalCode;
    }

    /**
     * Set the value of postalCode
     *
     * @return  self
     */
    public function setPostalCode($postalCode)
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    /**
     * Get the value of line1
     */
    public function getLine1()
    {
        return $this->line1;
    }

    /**
     * Set the value of line1
     *
     * @return  self
     */
    public function setLine1($line1)
    {
        $this->line1 = $line1;

        return $this;
    }
}

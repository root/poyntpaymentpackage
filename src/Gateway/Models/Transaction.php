<?php

namespace Devsolutions\PoyntPayment\Gateway\Models;

class Transaction
{
    protected $id;
    protected $status;
    protected $createdAt;
    protected $updatedAt;
    protected $businessType;
    protected $transmissionAtLocal;
    protected $storeDeviceId;
    protected $sourceApp;
    protected $mcc;
    protected $source;
    protected $storeId;
    protected $debit;
    protected TokenizedCard $card;
    protected $cardToken;
    protected $customerUserId;
    protected $action;
    protected $transactionAmount;
    protected $orderAmount;
    protected $tipAmount;
    protected $cashbackAmount;
    protected $currency;
    protected ProcessorResponse $processorResponse;

    /**
     * @param $id
     * @param $status
     * @param $createdAt
     * @param $updatedAt
     * @param $businessType
     * @param $transmissionAtLocal
     * @param $storeDeviceId
     * @param $sourceApp
     * @param $mcc
     * @param $source
     * @param $storeId
     * @param $debit
     * @param Card $card
     * @param $cardToken
     * @param $customerUserId
     * @param $action
     * @param $transactionAmount
     * @param $orderAmount
     * @param $tipAmount
     * @param $cashbackAmount
     * @param $currency
     * @param ProcessorResponse $processorResponse
     */
    public function __construct($id, $status, $createdAt, $updatedAt, $businessType, $transmissionAtLocal, $storeDeviceId, $sourceApp, $mcc, $source, $storeId, $debit, TokenizedCard $card, $cardToken, $customerUserId, $action, $transactionAmount, $orderAmount, $tipAmount, $cashbackAmount, $currency, ProcessorResponse $processorResponse)
    {
        $this->id = $id;
        $this->status = $status;
        $this->createdAt = $createdAt;
        $this->updatedAt = $updatedAt;
        $this->businessType = $businessType;
        $this->transmissionAtLocal = $transmissionAtLocal;
        $this->storeDeviceId = $storeDeviceId;
        $this->sourceApp = $sourceApp;
        $this->mcc = $mcc;
        $this->source = $source;
        $this->storeId = $storeId;
        $this->debit = $debit;
        $this->card = $card;
        $this->cardToken = $cardToken;
        $this->customerUserId = $customerUserId;
        $this->action = $action;
        $this->transactionAmount = $transactionAmount;
        $this->orderAmount = $orderAmount;
        $this->tipAmount = $tipAmount;
        $this->cashbackAmount = $cashbackAmount;
        $this->currency = $currency;
        $this->processorResponse = $processorResponse;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status): void
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     */
    public function setCreatedAt($createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param mixed $updatedAt
     */
    public function setUpdatedAt($updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return mixed
     */
    public function getBusinessType()
    {
        return $this->businessType;
    }

    /**
     * @param mixed $businessType
     */
    public function setBusinessType($businessType): void
    {
        $this->businessType = $businessType;
    }

    /**
     * @return mixed
     */
    public function getTransmissionAtLocal()
    {
        return $this->transmissionAtLocal;
    }

    /**
     * @param mixed $transmissionAtLocal
     */
    public function setTransmissionAtLocal($transmissionAtLocal): void
    {
        $this->transmissionAtLocal = $transmissionAtLocal;
    }

    /**
     * @return mixed
     */
    public function getStoreDeviceId()
    {
        return $this->storeDeviceId;
    }

    /**
     * @param mixed $storeDeviceId
     */
    public function setStoreDeviceId($storeDeviceId): void
    {
        $this->storeDeviceId = $storeDeviceId;
    }

    /**
     * @return mixed
     */
    public function getSourceApp()
    {
        return $this->sourceApp;
    }

    /**
     * @param mixed $sourceApp
     */
    public function setSourceApp($sourceApp): void
    {
        $this->sourceApp = $sourceApp;
    }

    /**
     * @return mixed
     */
    public function getMcc()
    {
        return $this->mcc;
    }

    /**
     * @param mixed $mcc
     */
    public function setMcc($mcc): void
    {
        $this->mcc = $mcc;
    }

    /**
     * @return mixed
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * @param mixed $source
     */
    public function setSource($source): void
    {
        $this->source = $source;
    }

    /**
     * @return mixed
     */
    public function getStoreId()
    {
        return $this->storeId;
    }

    /**
     * @param mixed $storeId
     */
    public function setStoreId($storeId): void
    {
        $this->storeId = $storeId;
    }

    /**
     * @return mixed
     */
    public function getDebit()
    {
        return $this->debit;
    }

    /**
     * @param mixed $debit
     */
    public function setDebit($debit): void
    {
        $this->debit = $debit;
    }

    /**
     * @return Card
     */
    public function getCard(): TokenizedCard
    {
        return $this->card;
    }

    /**
     * @param TokenizedCard $card
     */
    public function setCard(TokenizedCard $card): void
    {
        $this->card = $card;
    }

    /**
     * @return mixed
     */
    public function getCardToken()
    {
        return $this->cardToken;
    }

    /**
     * @param mixed $cardToken
     */
    public function setCardToken($cardToken): void
    {
        $this->cardToken = $cardToken;
    }

    /**
     * @return mixed
     */
    public function getCustomerUserId()
    {
        return $this->customerUserId;
    }

    /**
     * @param mixed $customerUserId
     */
    public function setCustomerUserId($customerUserId): void
    {
        $this->customerUserId = $customerUserId;
    }

    /**
     * @return mixed
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * @param mixed $action
     */
    public function setAction($action): void
    {
        $this->action = $action;
    }

    /**
     * @return mixed
     */
    public function getTransactionAmount()
    {
        return $this->transactionAmount;
    }

    /**
     * @param mixed $transactionAmount
     */
    public function setTransactionAmount($transactionAmount): void
    {
        $this->transactionAmount = $transactionAmount;
    }

    /**
     * @return mixed
     */
    public function getOrderAmount()
    {
        return $this->orderAmount;
    }

    /**
     * @param mixed $orderAmount
     */
    public function setOrderAmount($orderAmount): void
    {
        $this->orderAmount = $orderAmount;
    }

    /**
     * @return mixed
     */
    public function getTipAmount()
    {
        return $this->tipAmount;
    }

    /**
     * @param mixed $tipAmount
     */
    public function setTipAmount($tipAmount): void
    {
        $this->tipAmount = $tipAmount;
    }

    /**
     * @return mixed
     */
    public function getCashbackAmount()
    {
        return $this->cashbackAmount;
    }

    /**
     * @param mixed $cashbackAmount
     */
    public function setCashbackAmount($cashbackAmount): void
    {
        $this->cashbackAmount = $cashbackAmount;
    }

    /**
     * @return mixed
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param mixed $currency
     */
    public function setCurrency($currency): void
    {
        $this->currency = $currency;
    }

    /**
     * @return ProcessorResponse
     */
    public function getProcessorResponse(): ProcessorResponse
    {
        return $this->processorResponse;
    }

    /**
     * @param ProcessorResponse $processorResponse
     */
    public function setProcessorResponse(ProcessorResponse $processorResponse): void
    {
        $this->processorResponse = $processorResponse;
    }


}

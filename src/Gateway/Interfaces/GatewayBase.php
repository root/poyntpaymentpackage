<?php

namespace Devsolutions\PoyntPayment\Gateway\Interfaces;


use Devsolutions\PoyntPayment\Gateway\Models\Card;

interface GatewayBase
{

    public function generateAuthToken();
    public function getTokenizedCard(string $nonce, string $accessToken);
    public function createTransaction(string $paymentToken, $authToken, $chargedBy, $transactionAmount, $orderAmount, $transactionType);
    public function voidTransaction(string $authToken, string $transactionId) : array;
    public function captureTransaction(string $transactionId, string $authToken) : array;
    public function refundTransaction(string $authToken, string $transactionId, $transactionAmount, $orderAmount, $notes) : array;
    public function refreshAuthToken(string $refreshToken);
}

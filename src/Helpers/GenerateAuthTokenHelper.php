<?php

namespace Devsolutions\PoyntPayment\Helpers;


use Devsolutions\PoyntPayment\Gateway\Api\v1\AuthTokenJwt;
use Devsolutions\PoyntPayment\Gateway\Models\AuthToken;
use Devsolutions\PoyntPayment\Helpers\SelfSignedJWTHelper;

class GenerateAuthTokenHelper
{
    protected $environment; // app environment
    protected $privateKey; // PSA private key
    protected $businessId; // Poynt Business ID
    protected $appId; // Application ID


    public function __construct($environment, $privateKey, $businessId, $appId)
    {
        $this->environment = $environment;
        $this->privateKey = $privateKey;
        $this->businessId = $businessId;
        $this->appId = $appId;
    }

    public function authToken(): AuthToken
    {
        // create instance of the helper class Self Signed JWT
        $selfSignedTokenObj = new SelfSignedJWTHelper($this->environment, $this->privateKey, $this->appId);
        // create a self-signed JWT Token
        $selfSignedTokenObj->generateJwtSelfSignedToken();
        // create live auth token instance
        $authJwt = new AuthTokenJwt($this->environment, $selfSignedTokenObj->getSelfsignedToken());
        // return AuthToken Object
        return $authJwt->getAuthToken();
    }
}

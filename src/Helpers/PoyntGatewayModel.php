<?php

namespace Devsolutions\PoyntPayment\Helpers;

use Devsolutions\PoyntPayment\Gateway\Api\v1\ApiCardVoid;
use Devsolutions\PoyntPayment\Gateway\Api\v1\ApiRefundTransaction;
use Devsolutions\PoyntPayment\Gateway\Interfaces\GatewayBase;
use Devsolutions\PoyntPayment\Gateway\Models\AuthToken;
use Devsolutions\PoyntPayment\Gateway\Api\v1\ApiCardCapture;

class PoyntGatewayModel implements GatewayBase
{
    protected $businessId;
    protected $appId;
    protected $environment;
    protected $privateKey;

    /**
     * @param $businessId
     * @param $appId
     * @param $environment
     * @param $privateKey
     */
    public function __construct($businessId, $appId, $environment, $privateKey)
    {
        $this->businessId = $businessId;
        $this->appId = $appId;
        $this->environment = $environment;
        $this->privateKey = $privateKey;
    }

    /**
     * this method is used to generate Authentication Token of JWT and refresh token as well.
     *
     * @return AuthToken object.
     */
    public function generateAuthToken() : AuthToken
    {
        $authToken = new GenerateAuthTokenHelper($this->environment, $this->privateKey, $this->businessId, $this->appId);
        return $authToken->authToken();
    }

    public function getTokenizedCard(string $nonce, string $accessToken): \Devsolutions\PoyntPayment\Gateway\Models\TokenizedCard
    {
        $generateTokenizedCard = new TokenizeCardHelper($this->environment, $nonce, $this->businessId, $this->appId, $accessToken);
        return $generateTokenizedCard->tokenizedCard();
    }

    /**
     * this method is used to charge a payment token with certain amount in cents.
     *
     * @param string $paymentToken the payment token of object tokenized card
     * @param $authToken raw jwt authentication token
     * @param $chargedBy the name of the business will be added to the transaction
     * @param $transactionAmount the full price amount in cents
     * @param $orderAmount order price amount in cents
     * @param $transactionType enumrated value expect ChargeType Helper
     * @return \Devsolutions\PoyntPayment\Gateway\Models\Transaction transaction model
     */
    public function createTransaction(string $paymentToken, $authToken, $chargedBy, $transactionAmount, $orderAmount, $transactionType)
    {
        if($transactionType == 'SALE')
        {
            $chargeToken = new ChargedTokenHelper($this->businessId, $this->environment, $paymentToken, $authToken, $chargedBy, $transactionAmount, $orderAmount, $transactionType);
            return $chargeToken->chargeAToken();
        }
        else {
            $chargeToken = new ChargedTokenHelper($this->businessId, $this->environment, $paymentToken, $authToken, $chargedBy, $transactionAmount, $orderAmount, $transactionType);
            return $chargeToken->chargeAuthToken();
        }
    }

    public function voidTransaction(string $authToken, string $transactionId): array
    {
        $void = new ApiCardVoid($this->environment, $this->businessId, $this->appId, $authToken, $transactionId);
        return $void->voidCard();
    }

    public function captureTransaction(string $transactionId, string $authToken): array
    {
        $capture = new ApiCardCapture($this->environment,$this->businessId, $transactionId, $authToken);
        return $capture->cardCapture();
    }

    /**
     * this method is used to refund a transaction either a sales or auth. can be used as a full or partial refund
     *
     * @param string $authToken raw auth jwt access token
     * @param string $transactionId auth: capture transaction id | sales: transaction id
     * @param int $transactionAmount transaction amount in cents can be used for partial refund
     * @param int $orderAmount order amount in cents can be used for partial refund
     * @param string $notes notes for the dashboard
     * @return array response from poynt
     */
    public function refundTransaction(string $authToken, string $transactionId, $transactionAmount, $orderAmount, $notes) : array
    {
        $refund = new ApiRefundTransaction($this->environment, $authToken, $this->businessId, $transactionId, $transactionAmount, $orderAmount, $notes);
        return $refund->refundATransaction();
    }
    public function refreshAuthToken(string $refreshToken)
    {
        $authToken = new GenerateAuthTokenHelper($this->environment, $this->privateKey, $this->businessId, $this->appId);
        return $authToken->authToken();
    }
}

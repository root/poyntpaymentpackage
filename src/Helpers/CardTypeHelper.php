<?php

namespace Devsolutions\PoyntPayment\Helpers;

class CardTypeHelper
{
    /** @var string the Visa card type ID **/
    const CARD_TYPE_VISA = 'visa';

    /** @var string the MasterCard card type ID **/
    const CARD_TYPE_MASTERCARD = 'mastercard';

    /** @var string the American Express card type ID **/
    const CARD_TYPE_AMEX = 'amex';

    /** @var string the Diners Club card type ID **/
    const CARD_TYPE_DINERSCLUB = 'dinersclub';

    /** @var string the Discover card type ID **/
    const CARD_TYPE_DISCOVER = 'discover';

    /** @var string the JCB card type ID **/
    const CARD_TYPE_JCB = 'jcb';

    /** @var string the CarteBleue card type ID **/
    const CARD_TYPE_CARTEBLEUE = 'cartebleue';

    /** @var string the Maestro card type ID **/
    const CARD_TYPE_MAESTRO = 'maestro';

    /** @var string the Laser card type ID **/
    const CARD_TYPE_LASER = 'laser';

    public static function getType($accountNumber)
    {
        $types = array(
            self::CARD_TYPE_VISA       => '/^4/',
            self::CARD_TYPE_MASTERCARD => '/^(5[1-5]|2[2-7])/',
            self::CARD_TYPE_AMEX       => '/^3[47]/',
            self::CARD_TYPE_DINERSCLUB => '/^(36|38|30[0-5])/',
            self::CARD_TYPE_DISCOVER   => '/^(6011|65|64[4-9]|622)/',
            self::CARD_TYPE_JCB        => '/^35/',
            self::CARD_TYPE_MAESTRO    => '/^(5018|5020|5038|6304|6759|676[1-3])/',
            self::CARD_TYPE_LASER      => '/^(6706|6771|6709)/',
        );

        foreach ($types as $type => $pattern) {
            if (1 === preg_match($pattern, $accountNumber)) {
                return strtoupper($type);
            }
        }
    }
}

<?php

namespace Devsolutions\PoyntPayment\Helpers;

use Devsolutions\PoyntPayment\Gateway\Api\v1\ApiCardTokenization;
use Devsolutions\PoyntPayment\Gateway\Api\v1\CardTokenizationToNonce;
use Devsolutions\PoyntPayment\Gateway\Models\Card;
use Devsolutions\PoyntPayment\Gateway\Models\TokenizedCard;

class TokenizeCardHelper
{
    protected $environment;
    protected $nonce;
    protected $businessId;
    protected $appId;
    protected $authToken;
    /**
     * @param $environment
     * @param $businessId
     * @param $appId
     */
    public function __construct($environment, $nonce, $businessId, $appId, $authToken)
    {
        $this->environment = $environment;
        $this->nonce = $nonce;
        $this->businessId = $businessId;
        $this->appId = $appId;
        $this->authToken = $authToken;
    }

   /* protected function getNonce() : string
    {
        $nonceApiCall = new CardTokenizationToNonce($this->card, $this->environment, $this->businessId, $this->appId);
        return $nonceApiCall->getNonce();
    }*/

    public function tokenizedCard() : TokenizedCard
    {
        $tokenizeApiCall = new ApiCardTokenization($this->environment, $this->businessId, $this->nonce, $this->authToken);
        $tokenizeApiCall->tokenizeANonce();
        return $tokenizeApiCall->getTokenizedCard();
    }


}

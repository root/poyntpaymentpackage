<?php

namespace Devsolutions\PoyntPayment\Helpers;

use Devsolutions\PoyntPayment\Gateway\Api\v1\ApiChargingAuthorizeToken;
use Devsolutions\PoyntPayment\Gateway\Api\v1\ApiChargingSaleToken;
use Devsolutions\PoyntPayment\Gateway\Models\Transaction;

class ChargedTokenHelper
{
    protected $businessId;
    protected $environment;
    protected $cardToken;
    protected $authToken;
    protected $sourceApp;
    protected $transactionAmount;
    protected $orderAmount;
    protected $chargeToken;

    /**
     * @param $businessId
     * @param $environment
     * @param $cardToken
     * @param $authToken
     * @param $sourceApp
     * @param $transactionAmount
     * @param $orderAmount
     */
    public function __construct($businessId, $environment, $cardToken, $authToken, $sourceApp, $transactionAmount, $orderAmount, $chargeToken)
    {
        $this->businessId = $businessId;
        $this->environment = $environment;
        $this->cardToken = $cardToken;
        $this->authToken = $authToken;
        $this->sourceApp = $sourceApp;
        $this->transactionAmount = $transactionAmount;
        $this->orderAmount = $orderAmount;
        $this->chargeToken = $chargeToken;
    }


    public function chargeAToken()
    {
        $transactionApiCall = new ApiChargingSaleToken(
            $this->businessId,
            $this->environment,
            $this->cardToken,
            $this->authToken,
            $this->sourceApp,
            $this->transactionAmount,
            $this->orderAmount,
            $this->chargeToken
        );

        return $transactionApiCall->callChargeCard();
    }
    public function chargeAuthToken()
    {
        $transactionApiCall = new ApiChargingAuthorizeToken(
            $this->businessId,
            $this->environment,
            $this->cardToken,
            $this->authToken,
            $this->sourceApp,
            $this->transactionAmount,
            $this->orderAmount,
            $this->chargeToken
        );

        return $transactionApiCall->callChargeAuthCard();
    }
}

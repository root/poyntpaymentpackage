<?php

namespace Devsolutions\PoyntPayment\Helpers;

use Firebase\JWT\JWT;
use Ramsey\Uuid\Nonstandard\Uuid;

class SelfSignedJWTHelper
{
    protected $currentTime; // current timestamp
    protected $expireTime; // current timestamp + 300 second
    protected $environment; // the current environment
    protected $poyntBaseUrl; // poynt base url based on environment
    protected $appId; // poynt application id
    protected $uuid4; // unique id based on uuid version 4 structre
    protected $selfsignedToken; // JWT self signed token
    protected $privateKey; // RSA Private Key Value


    public function __construct(string $environment, string $privateKey, string $appId)
    {
        $this->currentTime = time();
        $this->expireTime = time() + 300;
        $this->environment = $environment;
        $this->appId = $appId;
        $this->uuid4 = Uuid::uuid4();
        $this->privateKey = $privateKey;
        $this->poyntBaseUrl = 'https://services.poynt.net';
    }

    /**
     * Get the value of envirnoment
     */
    public function getEnvironment(): string
    {
        return $this->environment;
    }

    /**
     * Set the value of environment
     *
     * @return  self
     */
    public function setEnvironment($environment): static
    {
        $this->environment = $environment;
        return $this;
    }

    /**
     * Set the value of privateKey
     *
     * @return  self
     */
    public function setPrivateKey($privateKey): static
    {
        $this->privateKey = $privateKey;
        return $this;
    }

    /**
     * Get the value of appId
     */
    public function getAppId(): string
    {
        return $this->appId;
    }

    /**
     * Set the value of appId
     *
     * @return  self
     */
    public function setAppId($appId): static
    {
        $this->appId = $appId;
        return $this;
    }

    public function generateJwtSelfSignedToken(): void
    {
        $payload = [
            'iss' => $this->appId,
            'sub' => $this->appId,
            'aud' => $this->poyntBaseUrl,
            'iat' => $this->currentTime,
            'nbf' => $this->currentTime,
            'exp' => $this->expireTime,
            'jti' => $this->uuid4,
        ];

        $this->selfsignedToken = JWT::encode($payload, $this->privateKey, 'RS256');
    }

    /**
     * Get the value of selfsignedToken
     */
    public function getSelfsignedToken()
    {
        return $this->selfsignedToken;
    }
}

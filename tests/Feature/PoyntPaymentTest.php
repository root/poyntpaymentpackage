<?php

namespace Tests\Feature;

use JetBrains\PhpStorm\NoReturn;
use Devsolutions\PoyntPayment\Gateway\Api\v1\ApiCardCapture;
use Devsolutions\PoyntPayment\Gateway\Api\v1\ApiCardTokenization;
use Devsolutions\PoyntPayment\Gateway\Api\v1\ApiCardVoid;
use Devsolutions\PoyntPayment\Gateway\Api\v1\ApiChargingAuthorizeToken;
use Devsolutions\PoyntPayment\Gateway\Api\v1\ApiChargingSaleToken;
use Devsolutions\PoyntPayment\Gateway\Api\v1\ApiRefundTransaction;
use Devsolutions\PoyntPayment\Gateway\Api\v1\AuthTokenJwt;
use Devsolutions\PoyntPayment\Gateway\Api\v1\RefreshAuthTokenJwt;
use Devsolutions\PoyntPayment\Gateway\Models\AuthToken;
use Devsolutions\PoyntPayment\Gateway\Models\RefreshAuthToken;
use Devsolutions\PoyntPayment\Gateway\Models\Transaction;
use Devsolutions\PoyntPayment\Helpers\SelfSignedJWTHelper;
use Mockery;
use Tests\TestCase;
class PoyntPaymentTest extends TestCase
{
    /**
     * A basic test example.
     */
    public function generateJwtSelfSignedToken()
    {
        $environment = config('poynt-keys.appEnv');
        $privateKey = config('poynt-keys.privateKey');
        $appId = config('poynt-keys.appId');

        $jwt = new SelfSignedJWTHelper($environment, $privateKey, $appId);
        $jwt->generateJwtSelfSignedToken();


        $token = $jwt->getSelfsignedToken();
        return $token;
    }
    public function getAuthToken(): AuthToken
    {
        $selfSignedJwt = $this->generateJwtSelfSignedToken();
        // Create an instance of the AuthTokenJwt class

        $authTokenJwt = new AuthTokenJwt(config('poynt-keys.appEnv'), $selfSignedJwt);
        // Call the getAuthToken method
        $authToken = $authTokenJwt->getAuthToken();

        // Add your assertions to verify the expected behavior
        $this->assertInstanceOf(AuthToken::class, $authToken);

        return $authToken;
    }
    public function testRefreshAuthToken()
    {
        $authToken = $this->getAuthToken();
        $refresh = $authToken->getRefreshToken();
        // Create an instance of the RefreshAuthTokenJwt class
        $refreshAuthTokenJwt = new RefreshAuthTokenJwt(config('poynt-keys.appEnv'), $refresh);
        // Call the getRefreshAuthToken method
        $authTokenRefreshed = $refreshAuthTokenJwt->getRefreshAuthToken();
        // Add your assertions to verify the expected behavior
        $this->assertInstanceOf(RefreshAuthToken::class, $authTokenRefreshed);

    }
    public function tearDown(): void
    {
        Mockery::close();
    }
    public function tokenizeCard(){

        $jwtAuthToken = $this->getAuthToken();
        // Create an instance of ApiCardTokenization
        $environment = 'production';
        $businessId = config('poynt-keys.businessId');
        $nonce = '6a98dd4b-c0ed-4ca6-b2c9-028b0892dde2';
        $accessToken = $jwtAuthToken->getAccessToken();


        $apiCardTokenization = new ApiCardTokenization($environment, $businessId, $nonce, $accessToken);

        // Call the tokenizeANonce method
        $apiCardTokenization->tokenizeANonce();

        return $apiCardTokenization
            ->getTokenizedCard()
            ->getPaymentToken();
    }
    public function callChargeSaleCard(): Transaction
    {
        $auth_token = $this->getAuthToken();
        $card_token = $this->tokenizeCard();

        // Prepare the necessary input values for the constructor
        $businessId = config('poynt-keys.businessId');
        $environment = config('poynt-keys.appEnv');
        $cardToken = $card_token;
        $authToken = $auth_token;
        $sourceApp = 'stripe';
        $transactionAmount = 100;
        $orderAmount = 100;
        $chargeType = 'type';

        // Create an instance of the ApiChargingSaleToken class
        $apiChargingToken = new ApiChargingSaleToken(
            $businessId,
            $environment,
            $cardToken,
            $authToken,
            $sourceApp,
            $transactionAmount,
            $orderAmount,
            $chargeType
        );
        // Make assertions on the callChargeCard method
        $transaction = $apiChargingToken->callChargeCard();

        $this->assertInstanceOf(Transaction::class, $transaction);
        // Add more assertions as needed based on the expected behavior of callChargeCard()
        // Additional assertions for specific properties or values
        $this->assertEquals($transactionAmount, $transaction->getTransactionAmount());
        // Add more assertions as needed for other properties or values
        return $transaction;
    }
    public function callChargeAuthCard(): Transaction
    {
        $auth_token = $this->getAuthToken();
        $card_token = $this->tokenizeCard();

        // Prepare the necessary input values for the constructor
        $businessId = config('poynt-keys.businessId');
        $environment = config('poynt-keys.appEnv');
        $cardToken = $card_token;
        $authToken = $auth_token;
        $sourceApp = 'stripe';
        $transactionAmount = 100;
        $orderAmount = 100;
        $chargeType = 'AUTH';

        // Create an instance of the ApiChargingSaleToken class
        $apiChargingToken = new ApiChargingAuthorizeToken(
            $businessId,
            $environment,
            $cardToken,
            $authToken,
            $sourceApp,
            $transactionAmount,
            $orderAmount,
            $chargeType
        );
        // Make assertions on the callChargeCard method
        $transaction = $apiChargingToken->callChargeAuthCard();

        $this->assertInstanceOf(Transaction::class, $transaction);
        // Add more assertions as needed based on the expected behavior of callChargeCard()
        // Additional assertions for specific properties or values
        $this->assertEquals($transactionAmount, $transaction->getTransactionAmount());
        // Add more assertions as needed for other properties or values
        return $transaction;
    }
    public function testRefundATransaction()
    {
        $auth_token = $this->getAuthToken();
        $transaction = $this->callChargeAuthCard();
        // Create an instance of ApiRefundTransaction with the necessary dependencies
        $refundTransaction = new ApiRefundTransaction(
            'production', // environment
            $auth_token,
            config('poynt-keys.businessId'),
            $transaction->getId(),
            $transaction->getTransactionAmount(),
            $transaction->getOrderAmount(),
            'your-notes'
        );

        // Call the refundATransaction method
        $result = $refundTransaction->refundATransaction();

        return $result;
    }
    #[NoReturn] public function testVoidCard()
    {
        $auth_token = $this->getAuthToken();
        $transaction = $this->callChargeSaleCard();
        // Create an instance of ApiCardVoid with the necessary dependencies
        $voidCard = new ApiCardVoid(
            'production', // environment
            config('poynt-keys.businessId'),
            config('poynt-keys.appId'),
            $auth_token,
            $transaction->getId(),
        );

        // Call the voidCard method
        $result = $voidCard->voidCard();

        dd($result);

        // Assert that the result is as expected
        //$this->assertEquals($expectedResult, $result);
    }
    #[NoReturn] public function testCardCapture()
    {
        $auth_token = $this->getAuthToken();
        $transaction = $this->callChargeAuthCard();
        // Create an instance of ApiCardCapture with the necessary dependencies
        $cardCapture = new ApiCardCapture(
            'production', // environment
            config('poynt-keys.businessId'),
            $auth_token,
            $transaction->getId(),
        );

        // Call the cardCapture method
        $result = $cardCapture->cardCapture();
        dd($result);

        // Assert that the result is as expected
        //$this->assertEquals($expectedResult, $result);
    }
}
